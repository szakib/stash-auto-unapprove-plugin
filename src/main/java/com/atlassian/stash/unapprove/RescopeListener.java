package com.atlassian.stash.unapprove;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.Changeset;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.event.pull.PullRequestRescopedEvent;
import com.atlassian.stash.idx.CommitIndex;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestParticipant;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.user.SecurityService;
import com.atlassian.stash.util.Operation;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageUtils;
import com.google.common.collect.Iterables;

public class RescopeListener {

    private final TransactionTemplate txTemplate;
    private final SecurityService securityService;
    private final PullRequestService pullRequestService;
    private final AutoUnapproveSettings autoUnapproveSettings;
    private final CommitService commitService;
    private final CommitIndex commitIndex;

    public RescopeListener(TransactionTemplate txTemplate, SecurityService securityService,
                           PullRequestService pullRequestService, AutoUnapproveSettings autoUnapproveSettings,
                           CommitService commitService, CommitIndex commitIndex) {
        this.txTemplate = txTemplate;
        this.securityService = securityService;
        this.pullRequestService = pullRequestService;
        this.autoUnapproveSettings = autoUnapproveSettings;
        this.commitService = commitService;
        this.commitIndex = commitIndex;
    }

    @EventListener
    public void onPullRequestRescoped(PullRequestRescopedEvent event) {
        final PullRequest pullRequest = event.getPullRequest();
        // only withdraw approval if the pull request's _source_ branch has changed, not its target.
        if (!event.getPreviousFromHash().equals(pullRequest.getFromRef().getLatestChangeset()) &&
                autoUnapproveSettings.isEnabled(pullRequest.getToRef().getRepository()) &&
                containsNonMerge(event.getPreviousFromHash(),
                                pullRequest.getFromRef().getLatestChangeset(),
                                pullRequest.getFromRef().getRepository())) {
            txTemplate.execute(new TransactionCallback<Void>() {
                @Override
                public Void doInTransaction() {
                    for (PullRequestParticipant participant : Iterables.concat(pullRequest.getReviewers(), pullRequest.getParticipants())) {
                        // the withdrawApproval method withdraws approval for the currently authenticated user,
                        // so use SecurityService to impersonate the reviewer we are withdrawing approval for
                        securityService.impersonating(participant.getUser(), "Unapproving pull-request on behalf of user").call(new Operation<Object, RuntimeException>() {
                            @SuppressWarnings("ConstantConditions")
                            @Override
                            public Object perform() {
                                return pullRequestService.withdrawApproval(
                                        pullRequest.getToRef().getRepository().getId(),
                                        pullRequest.getId()
                                );
                            }
                        });
                    }
                    return null;
                }
            });
        }
    }

    /**
     * Searches the commits between two points for "real changes" (non-merge commits) between them
     * @param fromHash first hash not to include
     * @param toHash last hash to include
     * @param repository the repo where the commits are
     * @return true if there is at least one non-merge commit, false otherwise
     */
    private boolean containsNonMerge(final String fromHash, final String toHash, final Repository repository)
    {
        final ChangesetsBetweenRequest request = new ChangesetsBetweenRequest.Builder(repository)
                .exclude(fromHash)
                .include(toHash)
                .build();
        final Page<Changeset> cs = commitService.getChangesetsBetween(request, PageUtils.newRequest(0, 9999));

        for(Changeset changeset: cs.getValues()) {
            //non-merge commits have max. 1 parent; only apply this to new commits
            if(changeset.getParents().size() < 2 && !commitIndex.isMemberOf(changeset.getId(), repository)) {
                return true;
            }
        }
        return false;
    }
}
