package com.atlassian.stash.unapprove;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.event.RepositoryDeletedEvent;
import com.atlassian.stash.repository.Repository;

public class AutoUnapproveSettings {

    public static final String PLUGIN_KEY = "com.atlassian.stash.plugin.stash-auto-unapprove-plugin";

    private final PluginSettings pluginSettings;

    public AutoUnapproveSettings(PluginSettingsFactory factory) {
        this.pluginSettings = factory.createSettingsForKey(PLUGIN_KEY);
    }

    @EventListener
    public void onRepositoryDeleted(RepositoryDeletedEvent event) {
        pluginSettings.remove(createKey(event.getRepository()));
    }

    public boolean isEnabled(Repository repository) {
        return pluginSettings.get(createKey(repository)) != null;
    }

    public void enableFor(Repository repository) {
        pluginSettings.put(createKey(repository), "1");
    }

    public void disableFor(Repository repository) {
        pluginSettings.remove(createKey(repository));
    }

    private String createKey(Repository repository) {
        return "repo." + repository.getId() + ".auto.unapprove";
    }

}
